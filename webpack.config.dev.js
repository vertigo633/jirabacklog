const webpack = require('webpack');
const path = require('path');
let jsonLoader = require('json-loader');

process.env.BABEL_ENV = process.env.npm_lifecycle_event;
module.exports = {
    entry: {
        jiraBacklog: [
            'babel-polyfill',
            './react/app.js'
        ]
    },

    output: {
        path: path.join(__dirname, 'react'),
        filename: '../../main/webapp/resources/js/[name].pack.js'
    },
    module: {
        rules: [
            {
                test: /\.(sass|scss)$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.js$/,
                exclude: [/node_modules/],
                use: [{
                    loader: 'babel-loader',
                    options: { presets: ['es2015'] },
                }],
            },
            {
                test: /\.json$/,
                use: [
                    'json-loader'
                ]
            },
        ]
    }
}