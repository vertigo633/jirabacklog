import ReactDOM from "react-dom";
import React from "react";
import {Provider} from 'react-redux'
import initStore from './store/Store'

$(document).ready(function () {
    const store = initStore();
    ReactDOM.render(
    <Provider store={store}>
    </Provider>,
    document.getElementById('dpm-entry'));
});